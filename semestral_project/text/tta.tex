\chapter{Transport Triggered Architecture}
\label{tta}

The proposed architecture for our FFT application is \zk{TTA} (\zkratkatext{TTA}). The architecture's principle and its advantages over the traditional \zk{VLIW} (\zkratkatext{VLIW}) are described in Section \ref{tta:overview}. A special tool for designing \zk{TTA} processors, developed by Department of Pervasive Computing at \zk{TUT}, is introduced in Section \ref{tta:tce}.

\section{TTA Overview}
\label{tta:overview}
Transport Triggered Architecture (\zk{TTA}) is a processor architecture where a designer has a full control over the data transports, specifying both a source and a destination. The instruction set has only one instruction: \textit{move}. To perform an operation (e.g. AND) we first \textit{move} an operand data to an operand input of a \zk{FU} (\zkratkatext{FU}). Operation itself is triggered by \textit{moving} a data to a trigger input of the \zk{FU}. After the operation is performed we can collect the result from the output of the \zk{FU}.

The data is transported using an \textit{interconnection network} which consists of a number of parallel buses connected to the inputs and outputs of \zk{FU}s. The number of buses is customizable as well as which \zk{FU} inputs/outputs are connected to which buses. Furthermore there is no limitation on the number of inputs and outputs of \zk{FU}s. Therefore we can customize the processor to a much greater degree than it is possible for example in a case of the \zk{VLIW} architecture.

The key property of \zk{TTA} is that we can \textit{move} data between functional units without the need to store them in \zkratkatext{RF}s (\zk{RF}). \zk{FU}s can distribute data between each other and thus reducing the pressure put on \zk{RF}s. We can have \zk{RF}s with a lower number of read and write ports and it is possible to implement them as traditional functional units. \cite{corporal}

The structure of \zk{TTA} suggests strong use of parallelism. Each bus can transport data independently to the others. The number of buses in the transportation network therefore equals the number of instructions which can be executed in one clock cycle.

Some \zk{FU}s have an access to a data memory. These are called \zk{LSU}s (\zkratkatext{LSU}s). The program instructions are stored in an \zk{IM} (\zkratkatext{IM}) and executed by \zk{GCU} (\zkratkatext{GCU}) which decodes them into the data transports.

The example architecture can be found in Fig. \ref{fig:tta_example}. The \zkratkatext{IC} consists of 6 buses. To these buses inputs and outputs of \zk{FU}s, \zk{GCU}, \zk{RF}s and \zk{LSU} are connected in a desired way. Connections to the buses are marked by a dot. Small asterlisks near the inputs of \zk{FU}s mark the trigger input. By writing data to this port the desired operation will be triggered. Each \zk{FU} can have multiple operations which are distinguished by an opcode provided with a data in the trigger port. These operations can be generic (ADD, MUL, SHIFT, etc.) or we have the possibility to design our own.

\begin{figure}[htb]
	\centering
	\resizebox{1\textwidth}{!}{\import{obrazky/}{tta_example.pdf_tex}}
	\caption{Example of a TTA processor configuration}
	\label{fig:tta_example}
\end{figure}

\section{TTA-Based Co-Design Environment}
\label{tta:tce}
\zkratkatext{TCE} (\zk{TCE}) \cite{tce} is a \zk{TTA} design toolset for a complete processor design and simulation from the high-level languages (C/C++) down to \zk{RTL} (\zkratkatext{RTL}) description (with support of VHDL or Verilog). The tool is available at \url{http://tce.cs.tut.fi}.

The typical simulation workflow is illustrated in Fig. \ref{fig:tce_workflow}. The core software used for an architecture definition is called "ProDe". ProDe is a tool for defining architecture's components (\zk{FU}s, \zk{RF}s, \zk{GCU}, etc. ) and connecting them to the \zkratkatext{IC}. It is possible to adjust data widths, specify address spaces and even assign \zk{HDL} (\zkratkatext{HDL}) descriprions to the \zk{FU}s. An output of ProDe is an architecture description file (.adf).

Each \zk{FU} perform one or more operations. These can be pre-defined or a designer can define his/her own operations in C/C++ language. Defining custom operations and asigning their behaviour source file can be done in a program "OSEd". In OSEd it is also possible to compile the behaviour source files and simulate the custom operations. OSEd stores operation descriptions in .opp files which can be then used in ProDe. Output of ProDe is an architecture definition file (.adf).

Next step in the simulation is compiling a target program. This program is written by a designer in a C/C++ or a TTA parallel assembly language. If any custom operations were defined they can referenced directly in the source. The "tcecc" compiler compiles the program against the output architecture from ProDe.

The compiled binary together with the .adf file are then fed into "Proxim" - the processor simulator. Proxim can be used also as a debugger tool for the program. It provides an overview about the register and memory usage, transport buses and \zk{FU} utilisation, it is possible to step the program and also see all data transports in each clock cycle.

For simulation purposes it is not necessary to define a hardware description of the operations. It is enough to specify (estimate) their latency in ProDe and then use the operatinos in a program source file. The simulator (Proxim) will use the latency information and assume that the operation is completed within the specified clock cycle range.

\begin{figure}[htb]
	\centering
	%\def\svgwidth{1\textwidth}
	%\import{obrazky/}{tce_workflow.pdf_tex}
	%\resizebox{0.5\textwidth}{!}{\import{obrazky/}{tce_workflow.pdf_tex}}
	\includegraphics[width=0.75\textwidth]{tce_workflow.png}
	\caption{Processor design and simulation workflow in TCE}
	\label{fig:tce_workflow}
\end{figure}
