%% Creator: Inkscape inkscape 0.91, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'twiddles.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{576.60103865bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,1)%
    \put(0,0){\includegraphics[width=\unitlength,page=1]{twiddles.pdf}}%
    \put(0.5039201,0.96506422){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$\operatorname{Re}(W_N^k)$}}}%
    \put(0.93341713,0.50720866){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$\operatorname{Im}(W_N^k)$}}}%
    \put(0.27026579,0.56379214){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{B4}}}%
    \put(0.39040649,0.68393271){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{B5}}}%
    \put(0.67898944,0.39436868){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{B0}}}%
    \put(0.56031083,0.27374678){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{B1}}}%
    \put(0.95733838,0.4255798){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{1}$}}}%
    \put(0.3904063,0.27374688){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{B2}}}%
    \put(0.2702657,0.39388762){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{B3}}}%
    \put(0.49674464,0.90456616){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{1}}}%
    \put(0.90838815,0.4611975){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{1}}}%
    \put(0.59169732,0.1968091){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$-jW_{\frac{N}{4}-k}^{k*}$}}}%
    \put(0.76720055,0.36896244){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_N^k$}}}%
    \put(0.35768015,0.1967092){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$-jW_{k-\frac{N}{4}}^{k}$}}}%
    \put(0.19128312,0.36216787){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$-W_{\frac{N}{2}-k}^{k*}$}}}%
    \put(0.17954049,0.60181486){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$-W_{k-\frac{N}{2}}^{k}$}}}%
    \put(0.3576926,0.75886096){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$jW_{\frac{3N}{4}-k}^{k*}$}}}%
    \put(0.95128984,0.38468005){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{2}$}}}%
    \put(0.66073617,0.0308619){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{12}$}}}%
    \put(0.70381874,0.05122207){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{11}$}}}%
    \put(0.93969847,0.33845109){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{3}$}}}%
    \put(0.92363002,0.29358214){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{4}$}}}%
    \put(0.90323995,0.25050558){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{5}$}}}%
    \put(0.87872524,0.20963653){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{6}$}}}%
    \put(0.85032267,0.17136851){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{7}$}}}%
    \put(0.81830635,0.13606972){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{8}$}}}%
    \put(0.78298512,0.1040799){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{9}$}}}%
    \put(0.74469965,0.07570646){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{10}$}}}%
    \put(0.61586671,0.01482139){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{13}$}}}%
    \put(0.56964212,0.00325433){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{14}$}}}%
    \put(0.52250706,-0.00372819){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{15}$}}}%
    \put(0.44532705,0.00096679){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{16}$}}}%
    \put(0.37977405,0.00345104){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{18}$}}}%
    \put(0.28910271,0.03090754){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{20}$}}}%
    \put(0.24602514,0.05127839){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{21}$}}}%
    \put(-0.00136407,0.38479738){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{30}$}}}%
    \put(0.02627333,0.29369257){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{28}$}}}%
    \put(0.02631901,0.66539011){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{36}$}}}%
    \put(0.09962638,0.78760389){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{39}$}}}%
    \put(0.20524955,0.883266){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{42}$}}}%
    \put(0.33408266,0.94415115){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{45}$}}}%
    \put(-0.00834314,0.5271488){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{33}$}}}%
    \put(0.04665279,0.25061101){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{27}$}}}%
    \put(0.07115744,0.20973587){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{26}$}}}%
    \put(0.1315583,0.13615423){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{24}$}}}%
    \put(0.20515022,0.07577277){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{22}$}}}%
    \put(0.95884684,0.4536364){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$W_{64}^{0}$}}}%
    \put(0.84274603,0.96510793){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$N = 16$}}}%
    \put(0.84274603,0.9304219){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$N = 32$}}}%
    \put(0.84274603,0.89573587){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$N = 64$}}}%
  \end{picture}%
\endgroup%
