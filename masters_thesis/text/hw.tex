%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Hardware Implementation Approach}
\label{hw}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The processor is going to be synthesized on an \zk{ASIC} technology in order to evaluate its power efficiency.
However, the processor's description is universal and allows an implementation on an \zk{FPGA}, too.
The two implementations will differ only in details and a types of memories used.
Before considering the \zk{ASIC} synthesis, \zk{FU}s need to be described and verified first.
An \zk{FPGA} device (ZYNQ XC7Z020-1CLG400C on a PYNQ-Z1 board) was used as a synthesis target for evaluating the \zk{FU}s.
At the time of writing this document, all custom \zk{FU}s are described in a VHDL language and their verification is in the process.
Because of that, this chapter gives an approach to the hardware design rather than definitive solutions.

Section \ref{hw:workflow} describes the process of designing a \zk{TTA} processor with the aid of \zk{TCE}.
In section \ref{hw:fus}, designs of custom \zk{FU}s are presented.
Section \ref{hw:mem} briefly describes memory types used and an access to them.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Design Workflow}
\label{hw:workflow}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The processor implementation workflow is depicted in Fig. \ref{fig:hw_workflow}.
First step is to describe all the necessary \zk{FU}s in a hardware definition language.
The basic ones are already provided by \zk{TCE}.
Using HDB Editor, a hardware database file is created.
This file is a library of all the possible \zk{HDL} implementations for the required \zk{FU}s.
It maps the \zk{HDL} description and parameters to the \zk{FU}s used in the architecture description (ProDe).
One \zk{FU} can contain multiple \zk{HDL} descriptions in the library (i.e. different latencies) so it is easy to switch between different implementations.
All \zk{HDL} descriptions use the same standardized interface which allows for a convenient mapping to the \zk{FU}s ports.

An architecture definition file is also needed for the generation.
It is obtained from ProDe during the timed simulation phase (section \ref{timed}).
Both the hardware database and the architecture definition are used as inputs to ProGe - Processor Generator.
ProGe's \zk{GUI} can also be accessed from ProDe.
In the \zk{GUI}, it is possible to specify a desired \zk{HDL} implementation for each \zk{FU} and register file as well as other parameters of the processor.
The configuration can be saved into an implementation definition file to save time selecting the parameters in the future.
ProGe generates a full \zk{HDL} description of the processor, optionally a testbench and a compilation and simulation scripts (for GHDL and ModelSim).

In order to be able to simulate a program on the architecture, the program's instruction memory image has to be generated.
This can be done in PIG (Program Image Generator).
It needs the program binary, also generated during the timed simulation phase.
The program creates the instruction memory image which is then used by the processor instruction decoder (part of the gcu \zk{FU}) to decode the instructions and execute them on the processor.

The generated processor can be then simulated and synthesized using third-party tools (such as GHDL, ModelSim, Xilinx Vivado, etc.).

\begin{figure}[tb]
	\centering
	\resizebox{1\textwidth}{!}{\import{obrazky/}{hw_workflow.pdf_tex}}
	\caption{Processor implementation workflow in TCE}
	\label{fig:hw_workflow}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Functional Units}
\label{hw:fus}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, a description of the hardware implementation of each custom \zk{FU} is given.
Some \zk{FU}s contain multiple possible implementations (differing i.e. in the latency) as it is easy to swap them in the processor generation process.
The latencies suggested in the timed simulation (chapter \ref{timed}) are based on \citep{pitkanen14} and serve as a starting point.
However, they might be a subject of change depending on the final requirements and restrictions.
The final \zk{FU}'s latency is a trade-off between speed on one side and a resource usage, power efficiency on the other.
For example the complex multiplier can be implemented with a lower latency than 3 but it would require using four multipliers instead of only two (see \ref{hw:cmul}).

The \zk{FU}s also have a lock and reset mechanism but they are left out from the block diagrams for clarity.
Generally, a unit accepts operands and computes values only when its trigger input port is triggered.
In case of units with a delayed output (such as the rotating register), the unit will keep outputting values every clock cycle even without triggering.
The reset signal resets all registers and states to zeros.

The standardized \zk{FU} interface ensures that all inputs are registered.
Thus a minimal latency of a \zk{FU} is one clock cycle (in case of a purely combinatorial \zk{FU}).

\subsection{Address Generator}
\label{hw:ag}

The address generator (Fig. \ref{fig:block_ag}) is used to compute the address of an input/output sample for the in-place computation.
First, a linear counter (\emph{lin\_idx}) is separated into two signals containing the information about the current stage (\zk{MSB}s of \emph{lin\_idx}) and a sample index within the current stage (\zk{LSB}s of \emph{lin\_idx}).
The number of bits where \emph{lin\_idx} is separated into \emph{stage} and \emph{idx} is determined by \emph{nexp} where \emph{nexp} is the current \zk{FFT} size as a power of two: $N = 2^{nexp}$.

The address generation itself is computed by splitting \emph{idx} into \emph{left} and \emph{right} parts and inserting two \zk{LSB}s of \emph{idx} between them (shifting the \emph{right} part two bits to the right).
The position  (\emph{pos}) where to insert the \emph{lsbs} is computed by shifting \emph{stage} by one bit to the left and subtracting it from \emph{nexp}.
The address computation is finished by concatenating \emph{left}, \emph{lsbs} and \emph{right} together (from \zk{MSB} to \zk{LSB}).

The final address is then multiplied by four (shift left by two positions).
This is architecture-specific and given by the current bus width and a \zkratkatext{MAU} (\zk{MAU}).
The proposed architecture's \zk{MAU} is 8 bits but the numbers are stored as 32-bit words, therefore one number consists of four \zk{MAU}s.

The \emph{base\_addr} input signal adds a static offset to the computed memory address.

\begin{figure}[tb]
	\centering
	\resizebox{0.7\textwidth}{!}{\import{obrazky/}{block_ag.pdf_tex}}
	\caption{Block diagram of an address generator}
	\label{fig:block_ag}
\end{figure}


\subsection{Twiddle Factor Generator}
\label{hw:tfg}

Twiddle factor generator (Fig. \ref{fig:block_tfg}) is probably the most complicated \zk{FU} used.
In general, first, a twiddle factor coefficient \emph{k} is computed.
It is converted into an address of a twiddle factor in an \zk{LUT} (see \ref{fft:memory}).
The twiddle factor is then fetched from the \zk{LUT} and manipulated in order to get the correct value.

As in the address generator, the input linear index is separated into \emph{stage} and \emph{idx}.
The base of \emph{k} is generated as modulo 4 (in case of radix-4 being computed) or modulo 2 (in case of radix-2) of \emph{idx}.
The current radix is determined by the \emph{rx2} signal which is computed by evaluating the \emph{nexp} and \emph{stage}.
When the computation is in the last stage and the \emph{nexp} is odd, radix-2 computation is being used and the \emph{rx2} flag is set to one.
The base of \emph{k} is then multiplied by a weight (\emph{w}) generated by reversing bit pairs of \emph{idx} (in a total length of \emph{nexp}).
The multiplication is not using an actual multiplier.
Instead, a constant multiplication principle is used since the base \emph{k} can be only either 0, 1, 2, or 3.
The weighted \emph{k} is then scaled by shifting left according to the current computation stage and maximum allowed \zk{FFT} size (currently $2^{14}$).
This scaling is necessary because an \zk{LUT} of a maximum size (2049) is used which is the native for \zk{FFT} of size $2^{14}$.
When computing smaller \zk{FFT}, some coefficients of larger \zk{FFT}s are not used and the \emph{k} needs to be scaled in order to access the right values.

The scaled \emph{k} is then converted into a read address for the \zk{LUT}.
As described in \ref{fft:memory}, only $N/8+1$ twiddle factors are stored in the \zk{LUT}.
The scaled \emph{k}, however, spans a larger range and needs to be converted to the \zk{LUT} address.
The maximum allowed ranges for a scaled \emph{k} and \emph{addr} are in the \uv{convert} box in the block diagram.
The converting unit also produces an opcode (\emph{opc}) to specify a manipulation of the twiddle factor retrieved from the \zk{LUT}.
The manipulation consists of a combination of the following operations (imaginary part in the diagram on Fig. \ref{fig:block_tfg} is denoted by a dotted line):
\begin{enumerate}
	\item Negate the real part
	\item Negate the imaginary part
	\item Swap real and imaginary parts
\end{enumerate}

\begin{figure}[tb]
	\centering
	\resizebox{0.6\textwidth}{!}{\import{obrazky/}{block_tfg.pdf_tex}}
	\caption{Block diagram of a twiddle factor generator}
	\label{fig:block_tfg}
\end{figure}


\clearpage
\subsection{Complex Adder}
\label{hw:cadd}

The complex adder performs the butterfly operation with four operands (\emph{a}, \emph{b}, \emph{c} and \emph{d}).
The unit consists of two parts: a sequential register and a combinatorial logic part (Fig. \ref{fig:block_cadd}).
The sequential part basically serves as a serial/parallel converter for a four-input combinatory complex adder.
The sequential part behaves like a state machine of four states.
The state is generated as a 2-bit linear counter.
It is incremented only when a new value is loaded into the \emph{t} port (every new load is indicated by the \emph{load} input signal).

Let's assume an input sequence of four numbers \emph{a}, \emph{b}, \emph{c} and \emph{d}, loaded after each other in the \emph{t} input port, each time triggering the \emph{load} port.
The state machine's behaviour can be described according to the value of the counter  (\emph{cnt}) as follows:
\begin{itemize}
	\item 0 : Store \emph{a} and \emph{rx2} into registers
	\item 1 : Store \emph{b} into a register
	\item 2 : Store \emph{c} into a register
	\item 3 : Load all register contents into the combinatory part  (\emph{d} is directly transferred - it is not registered)
\end{itemize}
The combinatory part computes four results in a sequence with the same operands (\emph{a}, \emph{b}, \emph{c} and \emph{d}).
After four clock cycles a new set of four operands is loaded.

On a diagram in a Fig. \ref{fig:block_cadd}, dotted lines represent imaginary parts of the input numbers.
Solid lines are the real parts.
An \emph{rx2} input signal provides the information about the current radix and together with \emph{cnt} they provide an opcode (\emph{opc}) for the combinatory part.
The opcode selects inputs of multiplexors, the negation of multiplexors' outputs and an add/subtract mode of the adders.

\begin{figure}
	\centering
	\resizebox{1\textwidth}{!}{\import{obrazky/}{block_cadd.pdf_tex}}
	\caption{Block diagram of a complex adder}
	\label{fig:block_cadd}
\end{figure}


\subsection{Complex Multiplier}
\label{hw:cmul}

Complex multiplier performs a multiplication of two complex numbers.
A pipelined version is shown on a block diagram on Fig. \ref{fig:block_cmul}.
The operation corresponds to the behaviour described by equation \ref{eq:cmul}.
However, the number of multipliers has been reduced to only two and the computation takes two clock cycles.

The unit's inputs are \emph{a} and \emph{b}.
First, the real part of the result is computed and after rounding and truncating it is delayed by a register.
In the next clock cycle, a select (\emph{sel}, one bit) signal is inverted and an imaginary part is computed.
The delayed real part and the immediate imaginary part are concatenated into a result (\emph{p}).

\begin{figure}
	\centering
	\resizebox{1\textwidth}{!}{\import{obrazky/}{block_cmul.pdf_tex}}
	\caption{Block diagram of a complex multiplier}
	\label{fig:block_cmul}
\end{figure}


\subsection{Rotating Register}
\label{hw:rotreg}

This \zk{FU} serves as a delay unit, delaying an input by 10 clock cycles.
The rotating register was chosen instead of a shift register because of a lower power consumption.
In a shift register, all values in all registers are replaced with the next values.
This constant replacing is inefficient.
In our case (length of 10), it would mean writing 10 values simultaneously every clock cycle.
Rotating register keeps values in their addresses.
An address counter is determining where a new value will be stored and from where an output value will be read in the current clock cycle.
Thus only one write and one read operations are performed per clock cycle.
Compared to a shift register there is the overhead of the modulo 10 counter.
However, when working with 32-bit numbers its influence on a power consumption is smaller than switching ten 32-bit values every clock cycle.

The unit's block diagram is on Fig. \ref{fig:block_rotreg}.
The address counter computes the write address.
The read address is one clock cycle before the write address.
This means that the read address pointer will be on the position of the current write address pointer 10 clock cycles later.

\begin{figure}
	\centering
	\resizebox{0.95\textwidth}{!}{\import{obrazky/}{block_rotreg.pdf_tex}}
	\caption{Block diagram of a rotating register}
	\label{fig:block_rotreg}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Memories}
\label{hw:mem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

There are three kinds of memories in the proposed architecture (Fig. \ref{fig:tta_arch}):
\begin{enumerate}
	\item Instruction memory
	\item Data memory
	\item Twiddle factor lookup table
\end{enumerate}
Their implementation can vary depending on the target platform and it is up to the user to specify them.
\zk{TCE} can generate the data and instruction memories for a testbench as synchronous \zk{SRAM} (\zkratkatext{SRAM}) memories.
Otherwise, \zk{TCE} can provide the data and instruction memory contents in several formats.
It can be in a form raw data to be put into a user's memory module, or as a premade VHDL array package.
The data and instruction memory were not in a consideration so far since the focus is, at the time of writing this, on implementing and verifying the custom \zk{FU}s.
The twiddle factor \zk{LUT} is a \zk{ROM} (\zkratkatext{ROM}).
At the time of the writing, it is implemented as a synchronous \zk{ROM} using two \zk{FPGA}'s \zk{BRAM} (\zkratkatext{BRAM}).
However, this will be changed into a distributed \zk{LUT} implementation for a better power efficiency and future \zk{ASIC} implementation.

During the kernel of the instruction schedule, two parallel memory accesses are performed each clock cycle (one read and one write).
Therefore, two load/store units need to be used.
\citep{pitkanen09} suggests using a parallel dual-port memory as the memory organization for the twiddle factor \zk{LUT}.
It was compared to a single dual-port memory and showed a smaller area and power consumption while maintaining the same performance.
Since the memory is divided into two separate memory modules acting like single-port memories, an address assigning mechanism needs to be used.
Therefore, an additional permutation logic needs to be inserted between the load/store units and memory modules.
This logic was provided by \zk{TUT} and will be used in the \zk{ASIC} synthesis.
